#include <stdio.h>
#include <stdlib.h>

int main()
{
    #define TITLE "DECIMAL TO RADIX-i converter"
    #define AUTHOR "Rasheeda"
    #define YEAR 2021

     for (int i=29;i>0; i--){ /*for i in the rage 29 (includes 29), print '*', decrement i
         after each printed '*' to print a total of 29 '*' */

        printf("%c",'*');
    }   /*print statements below, using format specifiers
        for string and integer constant values passed to the print methods*/
        printf("\n");
        printf("%s\n", TITLE);
        printf("Written by: %s\n", AUTHOR);
        printf("Date: %d\n",YEAR);

    for (int i=29;i>0; i--){

        printf("%c",'*');
    }

    int dec_num;
    printf("\n");
    printf("Enter a decimal number: ");//prompt to user
    scanf("%d", &dec_num); //taking in a decimal number from the user and storing it at a particular address
    printf("The number you have entered is %d", dec_num);
    return 0;
}
